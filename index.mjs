import chalk from 'chalk';

const xpValueTable = {
  20: 8,
  30: 7,
  40: 6,
  50: 5,
  60: 4,
  70: 3,
  80: 2,
  90: 1,
  100: 0,
}

const skills = [
  { physical: ['Athletics', 'Ride', 'Sail', 'Melee', 'Ranged', 'Brawl', 'Defend', 'Survival', 'Stealth', 'Crime', 'Awareness'] },
  { social: ['Commerce', 'Subterfuge', 'Empathy', 'Leadership', 'Charm', 'Performance', 'Animal Lore', 'Profession', 'Intimidation', 'Orate', 'Teaching'] },
  { mental: ['Library', 'Language', 'Battle', 'Customs', 'Medicine', 'Investigation', 'Statecraft', 'Wits', 'Craft', 'Household', 'Navigation'] },
  { magic: ['Alchemy', 'Meditate', 'Sense Magic', 'Spirit Combat', 'Spirit Dance', 'Spirit Travel', 'Lore', 'Worship', 'Ritual Cut', 'Prepare Corpse', 'Herbalist'] },
]

const skillValues = [
  { physical: [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10] },
  { social: [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10] },
  { mental: [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10] },
  { magic: [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10] },
]

console.log(skillValues[0].physical[11]);

const r = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

// ** CONFIG ** //
const xpPerGame = 10;
const gamesTotal = 10;

// ** END CONFIG ** //

function distributeInitialXp() {
  let primaryXP = 200;
  let secondaryXP = 100;
  let tertiaryXP = 100;
  let quaternaryXP = 100;
  const spendingXP = 10;

  // physical is always primary
  while (primaryXP > 0) {
    const skillIndex = r(0, 10);
    skillValues[0].physical[skillIndex] += spendingXP;
    primaryXP -= spendingXP;
  }

  // social is always secondary
  while (secondaryXP > 0) {
    const skillIndex = r(0, 10);
    skillValues[1].social[skillIndex] += spendingXP;
    secondaryXP -= spendingXP;
  }

  // mental is always tertiary
  while (tertiaryXP > 0) {
    const skillIndex = r(0, 10);
    skillValues[2].mental[skillIndex] += spendingXP;
    tertiaryXP -= spendingXP;
  }

  // magic is always quaternary
  while (quaternaryXP > 0) {
    const skillIndex = r(0, 10);
    skillValues[3].magic[skillIndex] += spendingXP;
    quaternaryXP -= spendingXP;
  }
}

function printChar() {
  console.log(chalk.bold('Physical Skills'));
  skills[0].physical.forEach((skill, index) => {
    console.log(`${skill}: ${skillValues[0].physical[index]}`);
  });

  console.log(chalk.bold('Social Skills'));
  skills[1].social.forEach((skill, index) => {
    console.log(`${skill}: ${skillValues[1].social[index]}`);
  });

  console.log(chalk.bold('Mental Skills'));
  skills[2].mental.forEach((skill, index) => {
    console.log(`${skill}: ${skillValues[2].mental[index]}`);
  });

  console.log(chalk.bold('Magic Skills'));
  skills[3].magic.forEach((skill, index) => {
    console.log(`${skill}: ${skillValues[3].magic[index]}`);
  });
}

function distributeGameXP() {
  for (let i = 0; i < gamesTotal; i++) {
    let xp = xpPerGame;
    while (xp > 0) {
      const randomSkillCategory = r(0, 3);
      const randomSkillIndex = r(0, 10);
      const skillValue = skillValues[randomSkillCategory][Object.keys(skillValues[randomSkillCategory])][randomSkillIndex];
      const skillGain = xpValueTable[skillValue - (skillValue % 10)];
      if (skillValue && skillGain && skillValue < 91) {
        skillValues[randomSkillCategory][Object.keys(skillValues[randomSkillCategory])[0]][randomSkillIndex] += skillGain;
        xp--;
      }
    }
  }
}

distributeInitialXp();
distributeGameXP();
printChar();
